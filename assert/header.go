package assert

// Header Assert that specific header is set with given value
func (a *Assert) Header(key string, value string) *Assert {
	values := a.R.Header[key]
	if len(values) == 0 {
		a.T.Errorf("Expected header %s to be set, it is not", key)

	} else if got := a.R.Header.Get(key); got != value {
		a.T.Errorf("Expected header %s to be set to '%s', got '%s'", key, value, got)
	}

	return a
}

// HeaderMissing Assert that specific header is not set.
func (a *Assert) HeaderMissing(key string) *Assert {
	value := a.R.Header.Get(key)
	if value != "" {
		a.T.Errorf("Expected header %s to be empty, got '%s'", key, value)
	}

	return a
}

// ContentType Assert that response is of specific `Content-Type`
func (a *Assert) ContentType(contentType string) *Assert {
	return a.Header("Content-Type", contentType)
}
