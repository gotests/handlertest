package handlertest

import (
	"net/http"
	"testing"
)

func TestCustomRequest(t *testing.T) {
	Call(expectHeader(t, "Allow-Origin", "*")).Request(func(request *http.Request) *http.Request {
		request.Header.Set("Allow-Origin", "*")
		return request
	}).Assert(new(testing.T))
}

func TestCustomRequestDeprecated(t *testing.T) {
	Call(expectHeader(t, "Allow-Origin", "*")).Custom(func(request *http.Request) {
		request.Header.Set("Allow-Origin", "*")
	}).Assert(new(testing.T))
}
