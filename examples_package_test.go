package handlertest_test

import (
	"fmt"
	"net/http"
	"testing"

	"gitlab.com/gotests/handlertest"
)

func Example_testListFilter() {
	ProductListControllerThatLikesCategoryB := func(w http.ResponseWriter, r *http.Request) {
		_, _ = w.Write([]byte(`[{"category":"b"}]`))
	}

	type Product struct {
		Category string
	}

	t := new(testing.T)

	// create your request
	handlertest.Call(ProductListControllerThatLikesCategoryB).GET("/products?category=a").
		// then assert your expectations
		Assert(t).
		Status(http.StatusOK).
		JSONMatches(func(t *testing.T, products []Product) {
			// unmarshalling of JSON objects is done for you
			if len(products) == 0 {
				t.Errorf("Expected to have some products returned")
			}
			for _, p := range products {
				if p.Category != "a" {
					t.Errorf("Expected filter to return products only of category %s, but got %s",
						"a", p.Category)

					fmt.Printf("Expected filter to return products only of category %s, but got %s",
						"a", p.Category)
				}
			}
		})

	// Output: Expected filter to return products only of category a, but got b
}

func Example_testUploadAttachments() {
	UploadAttachmentsController := func(w http.ResponseWriter, r *http.Request) {
		_ = r.ParseMultipartForm(1 << 10)

		fmt.Printf("post_id: %s\n", r.PostForm.Get("post_id"))
		fmt.Println(r.MultipartForm.File["files[]"][0].Filename)
	}

	t := new(testing.T)

	// create request
	handlertest.Call(UploadAttachmentsController).
		POST("/attachments").
		FormMultipartMap(map[string]string{
			"post_id": "1",
		}).
		File("files[]", "img1.jpg", "contents").
		// then assert your expectations
		Assert(t).
		Status(http.StatusCreated).
		ContentType("text/html")

	// Output:
	// post_id: 1
	// img1.jpg
}
