package handlertest_test

import (
	"fmt"
	"net/http"
	"os"
	"path/filepath"
	"testing"

	"gitlab.com/gotests/handlertest"
)

func ExampleRequest_FileReader() {
	YourHandler := func(w http.ResponseWriter, r *http.Request) {
		_ = r.ParseMultipartForm(1 << 10)

		fmt.Println(r.MultipartForm.File["files[]"][0].Filename)
	}

	t := new(testing.T)

	r, _ := os.Open(filepath.Join("testdata", "1.txt"))
	handlertest.Call(YourHandler).FileReader("files[]", "1.txt", r).Assert(t)

	// Output: 1.txt
}
