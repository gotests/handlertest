package handlertest

// Method Call your handler with a given method
func (r *Request) Method(method string) *Request {
	r.method = method
	return r
}

// GET Shorthand for Method("GET")
func (r *Request) GET(url string) *Request {
	r.method = "GET"
	return r.URL(url)
}

// POST Shorthand for Method("POST")
func (r *Request) POST(url string) *Request {
	r.method = "POST"
	return r.URL(url)
}
