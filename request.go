package handlertest

import (
	"io"
	"net/http"
	"net/http/httptest"
	"net/url"
	"testing"

	"gitlab.com/gotests/handlertest/assert"
)

// Request follows builder pattern to set how the request should look like.
type Request struct {
	handler http.HandlerFunc

	method  string
	url     string
	headers http.Header
	body    string
	files   map[string]map[string]io.Reader
	fields  url.Values
	custom  func(request *http.Request) *http.Request

	// TODO context https://gitlab.com/gotests/handlertest/issues/17
}

// Call your handler. This creates Request builder on which you can set
// how the request should look like by chaining methods.
func Call(handler http.HandlerFunc) *Request {
	return &Request{
		handler: handler,
		headers: http.Header{},
	}
}

// Assert Return object to specify how the http.Response should look like by chaining assertions.
func (r *Request) Assert(t *testing.T) *assert.Assert {
	return &assert.Assert{R: r.createResponse(t), T: t} //nolint:bodyclose // it is just for tests
}

// createResponse build http.Response using data set on chained Request
func (r *Request) createResponse(t *testing.T) *http.Response {
	// set method & url
	req, err := http.NewRequest(r.method, r.url, r.getBodyReader(t))
	if err != nil {
		t.Fatal(err.Error())
	}

	// set headers
	req.Header = r.headers

	// TODO Populate the request's context with our test data.
	//ctx := req.Context()
	//ctx = context.WithValue(ctx, "app.auth.token", "abc123")
	//ctx = context.WithValue(ctx, "app.user",
	//	&YourUser{ID: "qejqjq", Email: "user@example.com"})
	//
	//// Add our context to the request: note that WithContext returns a copy of
	//// the request, which we must assign.

	// TODO passing a http.Request pointer is a nice repeatable pattern
	// maybe I could use it internally instead of setting fields?
	// then all setters would have the same definition
	// req = req.WithContext(ctx)

	if r.custom != nil {
		req = r.custom(req)
	}

	// ============================
	// =========== TESTS ==========

	recorder := httptest.NewRecorder()

	r.handler.ServeHTTP(recorder, req)

	return recorder.Result()
}
