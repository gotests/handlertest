package handlertest

// URL Call handler with given URL
func (r *Request) URL(url string) *Request {
	r.url = url
	return r
}
